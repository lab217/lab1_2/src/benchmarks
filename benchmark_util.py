import random
from timeit import  default_timer


def create_lst(length):
        lst = [(1, random.randint(1, 100)) for i in range(1, length+10)]
        return lst

def correct_lines(f):
    result = 1
    lines = [line.strip() for line in f]
    lines_1 = [line for line in lines ]
    for line in lines_1:
        if line.find('swap') != -1:
            result = line.split('   ')
            break
    return result


def get_time(func):
    time_lst = []
    for i in range(2, 321, 1):
        start = default_timer()
        lst = create_lst(i)
        func(lst)
        end = default_timer()
        time_lst.append((end - start)*1000)
        print((end - start)*1000)

    return time_lst