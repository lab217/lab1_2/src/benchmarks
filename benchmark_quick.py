import cProfile
import pstats
from pstats import SortKey
from src import util
import benchmark_util
import matplotlib.pyplot as plt
from numpy import poly1d as np_poly1d, polyfit as np_polyfit

operation_lst = []
len_array = []

for i in range(2, 321, 1):
    lst = benchmark_util.create_lst(i)
    temp = 0
    for j in range(5):
        cProfile.run('util.quick_sort(lst)', 'stats.log')
        with open('../lab1/output.txt', 'w') as log_file_stream:
            p = pstats.Stats('stats.log', stream=log_file_stream)
            p.strip_dirs().sort_stats(SortKey.CALLS).print_stats()
        f = open('../lab1/output.txt')
        line = benchmark_util.correct_lines(f)
        f.close()
        temp += int(line)
    operation_lst.append(temp // 5)
    len_array.append(i)
print(operation_lst)
teory_operation = list(operation_lst)
teory_hard = [i**2 for i in range(len(teory_operation))]

time = benchmark_util.get_time(util.quick_sort)

fig, (ax1, ax2) = plt.subplots(2, 1)
fig.suptitle('"Быстрая сортировка"')

ax1.plot(len_array, operation_lst, c='green', label='Сортировка')
ax1.plot(len_array, teory_hard, c='red', label='Теор. сложность')
ax1.grid()
ax1.legend()
ax1.set_xlabel('Сложность алгоритма')
ax1.set_ylabel('Количество операций ')

fig.tight_layout()

trend = np_poly1d(np_polyfit(len_array, time, 2))
ax2.plot(len_array, trend(len_array), c='green')
ax2.set_xlabel('Зависимость времени от длины массива')
ax2.set_ylabel('Время с.')
ax2.grid()

fig.tight_layout()

plt.show()
